package hello.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import hello.mapper.StudentMapper;
import hello.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Autowired
	private StudentMapper mapper;
	
	@Cacheable(value = "studentCache", key = "#id")
	public Object findById(Long id) {
		System.out.println("--------------------->执行了方法");
		return mapper.findById(id);
	}
	
	@CacheEvict(value = "studentCache", key = "#id")
	public void updateSexById(Long id, String sex) {
		mapper.updateSexById(id, sex);
	}
	
}
