package hello.springdatajpaemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataJpaEmoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaEmoApplication.class, args);
    }
}
