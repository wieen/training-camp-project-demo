package hello.service.impl;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import hello.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Override
	@Cacheable("studentCache")
	public String findName() {
		System.out.println("--------------------->执行了方法");
		return "hello world";
	}
	
}
