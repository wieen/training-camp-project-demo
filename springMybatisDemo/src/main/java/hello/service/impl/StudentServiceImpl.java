package hello.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hello.entity.Student;
import hello.mapper.StudentMapper;
import hello.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Autowired
	private StudentMapper studentMapper;

	@Override
	public boolean login(String username, String password) {
		System.out.println("service");
		Student student = studentMapper.login(username, password);
		System.out.println(student.toString());
		return student != null;
	}

}
